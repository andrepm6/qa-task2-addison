import React from "react";
import { shallow } from "enzyme";
import Login from "../Login";

const userInput = jest.fn();
const passwordInput = jest.fn();

describe("Login component", () => {
  it("should match snapshot", () => {
    expect(shallow(<Login />)).toMatchSnapshot();
  });

  it("Should display TextInputs to login", () => {
    const wrapper = shallow(<Login />);

    expect(wrapper.find("TextInput").length).toBe(2);
  });

  it("Should display Text for error message", () => {
    const wrapper = shallow(<Login />);

    expect(wrapper.find("Text").length).toBe(1);
  });

  it("should change the username state", () => {
    const wrapper = shallow(<Login />);
    const user = "master";

    expect(wrapper.state("username")).toBe(null);

    wrapper
      .find("TextInput")
      .at(0)
      .simulate("ChangeText", user);

    expect(wrapper.state("username")).toBe(user);
  });

  it("should change the password state", () => {
    const wrapper = shallow(<Login />);
    const pwd = "ILoveReactNative";

    expect(wrapper.state("password")).toBe(null);

    const username = wrapper
      .find("TextInput")
      .at(1)
      .simulate("ChangeText", pwd);

    expect(wrapper.state("password")).toBe(pwd);
  });

  it("should change the errors state", () => {
    const wrapper = shallow(<Login />);
    const user = "master";
    const pwd = "ILoveJQuery";

    expect(wrapper.state("errors")).toBe(false);

    wrapper.find("TouchableOpacity").simulate("press");

    expect(wrapper.state("errors")).toBe(true);
  });
});
