import React from "react";
import { shallow } from "enzyme";
import Native from "../Native";

const navigation = { getParam: jest.fn() };

describe("Native component", () => {
  it("should match snapshot", () => {
    expect(shallow(<Native navigation={navigation} />)).toMatchSnapshot();
  });

  it("Should display a switch to show the balance", () => {
    const wrapper = shallow(<Native navigation={navigation} />);

    expect(wrapper.find("Switch").length).toBe(1);
  });

  it("Should display Text to show username and balance", () => {
    const wrapper = shallow(<Native navigation={navigation} />);

    expect(wrapper.find("Text").length).toBe(3);
  });

  it("should change the switch state", () => {
    const wrapper = shallow(<Native navigation={navigation} />);

    expect(wrapper.state("sw")).toBe(false);

    wrapper.find("Switch").simulate("ValueChange", "true");

    expect(wrapper.state("sw")).toBe(true);
  });
});
