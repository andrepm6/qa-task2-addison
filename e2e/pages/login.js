"use strict";

const loginPage = async function(user, pwd, login = true) {
  await browser.waitForExist("~Username Login Field", 5000);

  await browser.click("~Username Login Field");

  await browser.keys(user);

  await browser.click("~Password Login Field");

  await browser.keys(pwd);

  await browser.click("~Login Button");

  login
    ? await browser.waitForExist("~Login Button", 2000, true) // Checking if we're still in login sreen
    : await browser.waitForExist("~Login Error", 1000);
};

module.exports = loginPage;
