"use strict";

const loginPage = require("./pages/login");
const assert = require("chai").assert;

describe("Login fails", () => {
  before(function() {
    browser.reset();
  });

  it("should fail to login with invalid credentials", async () => {
    const credentials = {
      username: "master",
      password: "ILoveAngular"
    };

    await loginPage(credentials.username, credentials.password, false);

    const errorMsg = await browser.getText("~Login Error");

    assert.equal(errorMsg, "Wrong username or password");
  });
});

describe("Master test", () => {
  const credentials = {
    username: "master",
    password: "ILoveReactNative"
  };

  before(function() {
    browser.reset();
  });

  it("should login with master user", async () => {
    await loginPage(credentials.username, credentials.password);

    browser.waitForExist("~Welcome Message", 3000);

    const welcomeMsg = await browser.getText("~Welcome Message");

    assert.equal(welcomeMsg, "Welcome " + credentials.username);
  });

  it("should check the balance", async () => {
    await browser.click(`~Switch Button`);

    const balance = await browser.getText("~Balance Value");

    assert.equal(balance, "288.00 €");
  });
});

describe("Frontend user", () => {
  const credentials = {
    username: "frontend",
    password: "ItWillWork"
  };

  before(function() {
    browser.reset();
  });

  it("should login with frontend user", async () => {
    await loginPage(credentials.username, credentials.password);

    const contexts = await browser.contexts();

    await browser.context(contexts.value[1]); //setting webview context

    await browser.waitForExist("#username", 2000);

    const welcomeUser = await browser.getText("#username");

    assert.equal(welcomeUser, credentials.username);
  });

  it("should check the balance", async () => {
    await browser.click("#showBalance");

    const balance = await browser.getText("#balance");

    assert.equal(balance, "77.77 €");
  });
});
