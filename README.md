## About 

---

This task was completed using  the following tools:

* [appium](http://appium.io/)
* [webdriver.io](https://github.com/webdriverio/webdriverio)
* [mocha](https://mochajs.org/)
* [jest](https://jestjs.io/)
* [enzyme](https://airbnb.io/enzyme/)

End-to-end tests runs in both android and IOS.


###Strategy adopted 

* **E2E** - I've choosed webdriver.io instead [wd.js](https://github.com/admc/wd) because it has a wider documentation and seems way more flexible. Since the given scenarios are not too tricky and not too long, i've prefered to do it at the UI layer.

* **Component testing** - Created a few silly tests to exercise some of the react components (like snapshopts and states). I'm aware that is possible to make some integration test with enzyme, but unfortunately i hasn't much time to study in depth the API and the anatomy of an React Native application. Also, i wanted to tests the components passing invalid props to snapshot its stats, but i wasn't sure of how to do it withou having the propTypes explicited in the component.

####Other information

- Since accessibility properties are readed by screen readers, I've changed the values of the `TestID` and `AccessibilityLabel` for a more descriptive value. 

- I wished to set the component testing to run in cirlce CI as it would be very simple. But as i had to deliver this task in a private repistory, this was not possible.


### Installing


* ```npm install```

### Running


* ```npm test``` for "_component_" testing

* ```npm run appium:start``` and then ```npm run test:e2e``` for e2e testing


### Code standarts

---

* [Prettier:](https://www.npmjs.com/package/prettier) Code formatter - ```npm run format```
* [Husky:](https://www.npmjs.com/package/husky) Git hooks to prevent bad commits. Currently using precommit hook
