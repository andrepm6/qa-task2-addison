import { Platform } from "react-native";

const loginUsernameTestId = "Username Login Field";
const loginPasswordTestId = "Password Login Field";
const loginTestId = "Login Button";
const loginErrorTestId = "Login Error";
const switchTestId = "Switch Button";
const balanceViewTestId = "Balance View";
const balanceTextTestId = "Balance Value";
const homeViewTestId = "Home";
const nativeBackHomeTestId = "Back Home";
const welcomeMsgId = "Welcome Message";

const testID =
  Platform.OS === "ios"
    ? {
        username: {
          testID: loginUsernameTestId
        },
        password: {
          testID: loginPasswordTestId
        },
        login: {
          testID: loginTestId
        },
        error: {
          testID: loginErrorTestId
        },
        switch: {
          testID: switchTestId
        },
        balanceView: {
          testID: balanceViewTestId
        },
        balanceText: {
          testID: balanceTextTestId
        },
        homeView: {
          testID: homeViewTestId
        },
        nativeBackHome: {
          testID: nativeBackHomeTestId
        },
        welcomeMsg: {
          testID: welcomeMsgId
        }
      }
    : {
        username: {
          accessibilityLabel: loginUsernameTestId
        },
        password: {
          accessibilityLabel: loginPasswordTestId
        },
        login: {
          accessibilityLabel: loginTestId
        },
        error: {
          accessibilityLabel: loginErrorTestId
        },
        switch: {
          accessibilityLabel: switchTestId
        },
        balanceView: {
          accessibilityLabel: balanceViewTestId
        },
        balanceText: {
          accessibilityLabel: balanceTextTestId
        },
        homeView: {
          accessibilityLabel: homeViewTestId
        },
        nativeBackHome: {
          accessibilityLabel: nativeBackHomeTestId
        },
        welcomeMsg: {
          accessibilityLabel: welcomeMsgId
        }
      };

export default testID;
