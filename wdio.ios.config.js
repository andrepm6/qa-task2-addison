const host = '127.0.0.1'; 
const port = 4730;          

const waitforTimeout = 30 * 60000;
const commandTimeout = 30 * 60000;

exports.config = {

    debug: false,

    specs: [
	    './e2e/login-test.js'
    ],
    reporters: ['spec'],
    host: host,
    port: port,
    maxInstances: 1,
    capabilities: [
        {
            appiumVersion: '1.8.1',
            app: `${__dirname}/ios/build/Build/Products/Debug-iphonesimulator/QAReactNativeTechnicalTest.app`,
            browserName: '',
            platform: 'MAC',
            platformName: 'iOS',
            automationName: 'XCUITest',
            deviceName: 'iPhone 6',
            fullReset: false,
            noReset: true,
            waitforTimeout: waitforTimeout,
            commandTimeout: commandTimeout,
        }
    ],

    services: ['appium'],
    appium: {
        waitStartTime: 6000,
        waitforTimeout: waitforTimeout,
        command: 'appium',
        logFileName: 'appium.log',
        args: {
            address: host,
            port: port,
            commandTimeout: commandTimeout,
            sessionOverride: true,
            debugLogSpacing: true
        }
    },

    logLevel: 'silent',
    coloredLogs: true,
    framework: 'mocha',   
    mochaOpts: {
        timeout: 20000,
        ui: 'bdd'
    },    
};
